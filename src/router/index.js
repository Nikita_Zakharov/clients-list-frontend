import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../components/pages/Home.vue'
import Form from '../components/pages/Form.vue'
import Edit from '../components/pages/Edit.vue'

Vue.use(VueRouter)

export default new VueRouter({
    mode: 'history',
    base: '/',
    routes: [
        {
            component: Home,
            path: '/',
            name: 'home'
        },
        {
            component: Form,
            path: '/form',
            name: 'form'
        },
        {
            component: Edit,
            path: '/edit',
            name: 'edit'
        }
    ]
})