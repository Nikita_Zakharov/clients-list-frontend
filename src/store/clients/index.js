import mutations from './mutations'

export default {
    state: {
        list: []
    },
    mutations: mutations
}