export default {
    state: {
        name: "",
        data: null
    },
    getters: {},
    mutations: {
        'SET_MODAL_NAME'(state, value) {
            state.name = value
        },
        'SET_MODAL_DATA'(state, value) {
            state.data = value
        }
    },
    actions: {}
}